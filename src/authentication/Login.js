import React from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

const Login = ({ handleLogin }) => {

    let history = useHistory();

    return (
        <div>
            <Button onClick={handleLogin}>Login</Button>
            <Button onClick={() => history.push("/auth/register")}>Register</Button>
        </div>
    );
};

export default Login;