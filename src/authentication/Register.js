import React from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

const Register = (props) => {

    let history = useHistory();

    return (
        <div>
            <Button >Register Page</Button>
            <Button onClick={() => history.push("/auth/login")}>Login</Button>
        </div>
    );
};

export default Register;