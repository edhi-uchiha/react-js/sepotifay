import React from 'react';
import GenreListItem from './GenreListItem';

const ListGenre = ({ genres, editGenre, deleteGenre }) => {
    return (
        <div style={{ marginTop: 40 }}>
            <h3 style={{ textAlign: 'start', marginBottom: 10 }}>List Genre :</h3>
            {
                genres.map(genre => <GenreListItem key={genre.id} genre={genre} editGenre={editGenre} deleteGenre={deleteGenre} />)
            }
        </div>
    );
};

export default ListGenre;