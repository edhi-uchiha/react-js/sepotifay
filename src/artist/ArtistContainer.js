import React, { Component } from 'react';
import { getArtists, getArtistById, createArtist, updateArtist, deleteArtist } from './artistService';
import TableArtist from './TableArtist';
import DetailsArtist from './DetailsArtist';
import { Button } from 'react-bootstrap';
import ToastComponent from '../components/ToastComponent';

class ArtistContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            artists: [],
            showDetails: false,
            selectedArtist: {},
            disableForm: true,
            showToast: {
                visible: false,
                title: "",
                message: "",
                color: ""
            }
        }
    }

    componentDidMount() {
        this.loadData();
    }

    loadData = () => {

        getArtists().then(response => {

            if (response.status.code === 200) {
                this.setState({ ...this.state, artists: response.data });
            }
        });
    }

    getArtistById = (id) => {
        getArtistById(id).then(res => {
            if (res.status.code === 200) {
                this.setState({ ...this.state, selectedArtist: res.data });
            }
        })
    }

    editArtist = () => {

        updateArtist({ ...this.state.selectedArtist }).then(res => {
            if (res.status.code === 200) {
                this.showToast({ title: "Update Artist", message: `Artist successfully updated.`, color: 'green' });
                this.loadData();
            }
        })
    }

    createArtist = (files) => {

        const payload = { ...this.state.selectedArtist, photo: "" };
        createArtist({ files, payload })
            .then(res => {
                if (res.status.code === 201) {
                    this.showToast({ title: "Create Artist", message: `Artist successfully created.`, color: 'green' });
                    this.loadData();
                }
            })

    }

    deleteArtist = (id) => {

        deleteArtist(id).then(res => {
            if (res.status === 204) {
                this.showToast({ title: "Delete Artist", message: `Artist successfully deleted.`, color: 'green' });
                this.loadData();
            }
        })

    }

    showDetails = (artist) => {
        this.setState({
            ...this.state,
            showDetails: true,
            selectedArtist: { ...artist }
        })
    }

    hideDetails = () => {
        this.setState({
            ...this.state,
            showDetails: false,
            disableForm: true,
        })
    }

    onEditingOn = () => {
        this.setState({
            ...this.state,
            disableForm: !this.state.disableForm
        })
    }

    onCreate = () => {

        this.setState({
            ...this.state,
            showDetails: true,
            disableForm: false,
            selectedArtist: {}
        })
    }

    handleChangeInput = (event) => {
        let setValue = event.target.name;
        if (setValue === 'MALE' || setValue === 'FEMALE') {
            this.setState({
                ...this.state,
                selectedArtist: {
                    ...this.state.selectedArtist,
                    gender: setValue
                }
            })
        } else {
            this.setState({
                ...this.state,
                selectedArtist: { ...this.state.selectedArtist, [setValue]: event.target.value }
            })
        }
    }

    showToast = ({ title, message, color }) => {
        this.setState({
            ...this.state, showToast: {
                visible: true,
                title,
                message,
                color
            }
        })
    }

    hideToast = () => {
        this.setState({
            ...this.state, showToast: {
                ...this.state.showToast,
                visible: false,
            }
        })
    }

    render() {

        return (
            <div>
                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                    <h2>Artist Page</h2>
                    <Button variant="success" onClick={this.onCreate}>
                        New Artist
                    </Button>
                </div>
                <hr
                    style={{
                        color: '#aaa',
                        height: 5
                    }} />
                <TableArtist artists={this.state.artists} showDetails={this.showDetails} removeArtist={this.deleteArtist} />
                <DetailsArtist
                    show={this.state.showDetails}
                    artist={this.state.selectedArtist}
                    onHide={this.hideDetails}
                    disable={this.state.disableForm}
                    onEditingOn={this.onEditingOn}
                    handleChangeInput={this.handleChangeInput}
                    onCreate={this.createArtist}
                    onUpdate={this.editArtist}
                />
                <ToastComponent toast={this.state.showToast} hideToast={this.hideToast} />
            </div>
        );
    }
}

export default ArtistContainer;