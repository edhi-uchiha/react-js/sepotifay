const baseUrl = '/sepotifay/api/artists';

// getAll
const getArtists = async () => {
    const artists = await fetch(baseUrl);
    return await artists.json();
}

const getArtistById = async (id) => {
    const artists = await fetch(`${baseUrl}/${id}`);
    return await artists.json();
}

const createArtist = async ({ files, payload }) => {

    const formData = new FormData()

    formData.append('file', files, files.name);
    formData.append('body', JSON.stringify(payload));

    const res = await fetch( baseUrl, {
        method: 'POST',
        body: formData
    })

    return await res.json();
}

const updateArtist = async (form) => {
    const artist = await fetch(baseUrl, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(form)
    })

    return await artist.json();
}

const deleteArtist = async (id) => {
    const res = await fetch(baseUrl + `/${id}`, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    })

    return res;
}

const getImageAttachment = async (id) => {

    const res = await fetch(`${baseUrl}/image/${id}`);
    return res.blob();

}

export { getArtists, getArtistById, createArtist, updateArtist, deleteArtist, getImageAttachment };