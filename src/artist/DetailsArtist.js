import React, { useEffect, useState } from 'react';
import { Form, Modal, Col, Button } from 'react-bootstrap';
import { getImageAttachment } from './artistService';
import ButtonUpload from '../components/ButtonUpload';
import { Link } from 'react-router-dom';

const DetailsArtist = (props) => {

    const {
        show,
        onHide,
        disable,
        handleChangeInput,
        onEditingOn,
        onCreate,
        onUpdate,
        artist: { id, name, gender, debutYear, biography } } = props;

    const [photo, setPhoto] = useState("");

    const onFileChange = e => {
        setPhoto(e.target.files[0]);
    };

    useEffect(() => {

        if (show && id) {
            getImageAttachment(id).then(res => {
                const outside = URL.createObjectURL(res);
                setPhoto(outside);
            }).catch(err => {
                console.log(err);
            })
        }
        // eslint-disable-next-line
    }, [show])

    const editOrSave = () => {
        if (!id) {
            onCreate(photo);
            onHide();
        } else {
            onUpdate();
            onHide();
        }
    }

    const hideModal = () => {
        setPhoto("");
        onHide();
    }

    return (
        <Modal
            show={show}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
            onEscapeKeyDown={hideModal}
            onHide={hideModal}>

            <Modal.Header closeButton={true}>
                <Modal.Title id="contained-modal-title-vcenter">
                    Artist
                    </Modal.Title>
            </Modal.Header>

            <Modal.Body>
                <Form>
                    <Form.Row>

                        <Form.Group as={Col} sm={5}>
                            <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', paddingLeft: 16, paddingRight: 16, marginRight: 10 }}>
                                {!id ?
                                    <ButtonUpload onChange={onFileChange} label="Upload" disable={disable} /> :
                                    <ButtonUpload onChange={onFileChange} label="Edit" photo={photo} disable={disable} />
                                }
                            </div>
                        </Form.Group>

                        <Form.Group as={Col} sm={7}>
                            <Form.Row>
                                <Form.Group as={Col} sm={8} controlId="formArtistName">
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control
                                        name="name"
                                        value={name || ""}
                                        disabled={disable}
                                        onChange={handleChangeInput}
                                        placeholder="Enter artist name" />
                                </Form.Group>

                                <Form.Group as={Col} controlId="formGridDebutYear">
                                    <Form.Label>Debut Year</Form.Label>
                                    <Form.Control as="select" name="debutYear" value={debutYear} disabled={disable} onChange={handleChangeInput}>
                                        <option>{debutYear || 2000}</option>
                                        <option>2000</option>
                                        <option>2001</option>
                                        <option>2002</option>
                                        <option>2003</option>
                                        <option>2004</option>
                                        <option>2005</option>
                                        <option>2006</option>
                                        <option>2007</option>
                                        <option>2008</option>
                                        <option>2009</option>
                                    </Form.Control>
                                </Form.Group>
                            </Form.Row>

                            <Form.Label>Gender</Form.Label>
                            <Form.Row>
                                <Form.Group as={Col} controlId="formGridGenderFeMale">
                                    <Form.Check name="FEMALE" type="checkbox" label="FEMALE" disabled={disable} onChange={handleChangeInput} checked={(gender === 'FEMALE')} />
                                </Form.Group>
                                <Form.Group as={Col} controlId="formGridGenderMale">
                                    <Form.Check name="MALE" type="checkbox" label="MALE" disabled={disable} onChange={handleChangeInput} checked={(gender === 'MALE')} />
                                </Form.Group>
                            </Form.Row>

                            <Form.Group controlId="formArtistBiography">
                                <Form.Label>Biography</Form.Label>
                                <Form.Control name="biography" as="textarea" rows={3} value={biography} disabled={disable} onChange={handleChangeInput} />
                            </Form.Group>
                        </Form.Group>

                    </Form.Row>
                </Form>
            </Modal.Body>

            <Modal.Footer>

                <Button
                    onClick={disable ? onEditingOn : () => editOrSave('create')}
                    disabled={(!photo || !name || !gender || !debutYear || !biography)}>
                    {disable ? 'Edit' : 'Save'}
                </Button>

                <Button as={Link} to={`/artists`} onClick={hideModal}>Close</Button>

            </Modal.Footer>
        </Modal>
    );
};

export default DetailsArtist;
