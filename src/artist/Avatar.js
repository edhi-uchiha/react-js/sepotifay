import React, { useState, useEffect } from 'react';
import { Image } from 'react-bootstrap';
import { getImageAttachment } from './artistService';

const Avatar = ({ id }) => {

    const [photo, setPhoto] = useState("");

    useEffect(() => {

        getImageAttachment(id).then(res => {
            const outside = URL.createObjectURL(res);
            setPhoto(outside);
        }).catch(err => {
            console.log(err);
        })
        // eslint-disable-next-line
    }, [])

    return (
        <Image src={photo} rounded style={{ height: 50, width: 50 }} />
    );
};

export default Avatar;