import React, { useState } from 'react';
import { Table, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt, faEdit } from '@fortawesome/free-regular-svg-icons';
import Avatar from './Avatar';
// import ConfirmDelete from '../components/ConfirmDelete';
import { Link } from 'react-router-dom';

const TableArtist = (props) => {

    const { artists, showDetails, removeArtist } = props;
    const [modalVisible, setModalVisible] = useState(false);
    const [id, setId] = useState("");

    const deleteConfirmed = () => {
        removeArtist(id);
        setModalVisible(false);
    }

    const deleteCanceled = () => {
        setModalVisible(false);
    }

    const onDelete = (id) => {
        setModalVisible(true);
        setId(id);
    }

    return (
        <div>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Biography</th>
                        <th>Gender</th>
                        <th>Debut Year</th>
                        <th>Photo</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        artists.map((artist, index) => (
                            <tr key={artist.id}>
                                <td>{index + 1}</td>
                                <td>{artist.name}</td>
                                <td>{artist.biography}</td>
                                <td>{artist.gender}</td>
                                <td>{artist.debutYear}</td>
                                <td><Avatar id={artist.id} /></td>
                                <td>
                                    <Button as={Link} to={`/artists/details/${artist.id}`} onClick={() => showDetails(artist)} variant="info">
                                        <FontAwesomeIcon icon={faEdit} />
                                    </Button>
                                    <Button onClick={() => onDelete(artist.id)} variant="danger" style={{ marginLeft: 12 }}>
                                        <FontAwesomeIcon icon={faTrashAlt} />
                                    </Button>
                                </td>

                            </tr>
                        ))
                    }
                </tbody>
            </Table>
            {/* <ConfirmDelete
                modalVisible={modalVisible}
                title="Delete Artist"
                body="Are you sure to delete this artist ?"
                handleOk={deleteConfirmed}
                handleCancel={deleteCanceled}
            /> */}
        </div>
    );
};

export default TableArtist;
