import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import Login from '../authentication/Login';
import Register from '../authentication/Register';

const AuthScreen = (props) => (
    <Switch>
        <Route path="/" exact>
            <Redirect to={{ pathname: "/auth/login", state: props.location }} />
        </Route>
        <Route path="/auth/login" exact>
            <Login handleLogin={props.handleLogin} />
        </Route>
        <Route path="/auth/register" exact>
            <Register {...props} handleLogin={props.handleLogin} />
        </Route>
        <Route path="*">
            <Redirect to={{ pathname: "/auth/login", state: props.location }} />
        </Route>
    </Switch>
)

export default AuthScreen;