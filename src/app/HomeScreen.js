import React from 'react';
import Header from '../components/Header';
import { Container } from 'react-bootstrap';
import { Switch, Route, Redirect } from 'react-router-dom';
import Home from '../home/Home';
import GenreContainer from '../genre/GenreContainer';
import NotFoundPage from '../home/NotFoundPage';
import ArtistContainer from '../artist/ArtistContainer';
import DetailsArtist from '../artist/DetailsArtist';

const HomeScreen = (props) => (
    <>
        <Header />
        <Container style={{ paddingTop: 24 }}>
            <Switch>
                <Route path="/auth/login" exact>
                    <Redirect to={{ pathname: "/", state: props.location }} />
                </Route>
                <Route strict path="/" exact>
                    <Home handleLogout={props.handleLogout} />
                </Route>
                <Route path="/genres" exact>
                    <GenreContainer />
                </Route>
                <Route path={["/artists", "/artists/details/:id"]} exact children={DetailsArtist}>
                    <ArtistContainer />
                </Route>
                <Route path="*">
                    <NotFoundPage />
                </Route>
            </Switch>
        </Container>
    </>
)

export default HomeScreen;
