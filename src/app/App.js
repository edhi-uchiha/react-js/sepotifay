import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { HashRouter as Router } from 'react-router-dom';
import HomeScreen from './HomeScreen';
import AuthScreen from './AuthScreen';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      auth: "",
      CurrentPage: <AuthScreen handleLogin={this.handleLogin} />
    }
  }

  componentDidMount() {
    const auth = localStorage.getItem("userData");
    if (auth) {
      this.setState({ ...this.state, CurrentPage: <HomeScreen handleLogout={this.handleLogout} /> });
    }

    console.log("LOC",process.env.BASE_URL_API);
  }

  handleLogin = () => {

    localStorage.setItem("userData", 'edhi.uchiha')
    this.setState({ ...this.state, CurrentPage: <HomeScreen handleLogout={this.handleLogout} /> });

  }

  handleLogout = () => {

    localStorage.removeItem("userData")
    this.setState({ ...this.state, CurrentPage: <AuthScreen handleLogin={this.handleLogin} /> });

  }

  render() {

    return (
      <Router>
        <div className="App">
          {this.state.CurrentPage}
        </div>
      </Router>
    );
  }
}

export default App;