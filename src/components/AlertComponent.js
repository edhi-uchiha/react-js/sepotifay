import React from 'react';
import { Alert, Button } from 'react-bootstrap';

const AlertComponent = ({ toast: { title, message, visible }, hideToast }) => {
    return (
        <div>
            <Alert show={visible} variant="success" style={{ width: 500 }}>
                <Alert.Heading>{title}</Alert.Heading>
                <p>
                    {message}
                </p>
                <hr />
                <div className="d-flex justify-content-end">
                    <Button onClick={hideToast} variant="outline-success">Close
                </Button>
                </div>
            </Alert>
        </div>
    );
};

export default AlertComponent;