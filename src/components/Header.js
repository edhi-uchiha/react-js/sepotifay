import React from 'react';
import logo from '../logo.svg';
import { Link } from 'react-router-dom';
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';

const Header = () => {

    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand as={Link} to={"/"}>
                <img src={logo} className="App-logo" alt="logo" />
            </Navbar.Brand>
            <Nav className="mr-auto">
                <Nav.Link as={Link} to={"/"}>Home</Nav.Link>
                <Nav.Link as={Link} to={"/genres"} >Genres</Nav.Link>
                <Nav.Link as={Link} to={"/artists"}>Artists</Nav.Link>
            </Nav>
            <Form inline>
                <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                <Button variant="outline-info">Search</Button>
            </Form>
        </Navbar>
    );
};

export default Header;