import React from 'react';
import { Toast } from 'react-bootstrap';

const ToastComponent = ({ toast: { title, message, visible, color }, hideToast }) => {
    return (
        <div style={{ display: 'flex', position: 'absolute', right: 30, bottom: 30 }}>
            <Toast onClose={hideToast} show={visible} delay={3000} autohide style={{ backgroundColor: 'white' }}>
                <Toast.Header style={{ backgroundColor: color }}>
                    <strong className="mr-auto" style={{ color: 'white' }}>{title}</strong>
                </Toast.Header>
                <Toast.Body style={{ width: 500 }}>
                    <p style={{ textAlign: 'left', color, fontSize: 14 }}>{message}</p>
                </Toast.Body>
            </Toast>
        </div>
    );
};

export default ToastComponent;